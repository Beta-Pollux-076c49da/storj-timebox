package repo

import (
	"github.com/google/uuid"
	"github.com/zeebo/assert"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"testing"
	"time"
	"timebox/model"
)

func TestGC(t *testing.T) {
	db, err := setupTestDatabase()
	assert.NoError(t, err)
	err = db.AutoMigrate(&model.File{})
	assert.NoError(t, err)
	repo := FilesRepository{DB: db}
	expiredFileIDs := make([]uuid.UUID, 0)

	for _, i := range []int{-2, -3, -4, 5, 10} {
		var expiresAt = time.Now()
		fileID := uuid.New()
		expiresAt = expiresAt.Add(time.Duration(i) * time.Minute)
		createdAt := expiresAt.Add(time.Duration(-i) * time.Minute)
		if i < 0 {
			expiredFileIDs = append(expiredFileIDs, fileID)
		}
		f := model.File{
			ID:           fileID,
			Path:         "/file",
			Alias:        nil,
			MaxDownloads: 4,
			Downloads:    0,
			Size:         0,
			ContentType:  "",
			ExpiresAt:    expiresAt,
			CreatedAt:    createdAt,
			DeletedAt:    nil,
		}
		err := repo.NewFile(&f)
		assert.NoError(t, err)
	}
	//Files with downloads >= threshold
	for _, i := range []int{5, 10, 20, 40} {
		var expiresAt = time.Now()
		fileID := uuid.New()
		expiresAt = expiresAt.Add(time.Duration(i) * time.Minute)
		createdAt := expiresAt.Add(time.Duration(-i) * time.Minute)
		expiredFileIDs = append(expiredFileIDs, fileID)
		f := model.File{
			ID:           fileID,
			Path:         "/file",
			Alias:        nil,
			MaxDownloads: 4,
			Downloads:    4,
			Size:         0,
			ContentType:  "",
			ExpiresAt:    expiresAt,
			CreatedAt:    createdAt,
			DeletedAt:    nil,
		}
		err := repo.NewFile(&f)
		assert.NoError(t, err)
	}
	gcFileIDs := make([]uuid.UUID, 0)
	for _, f := range repo.GC() {
		gcFileIDs = append(gcFileIDs, f.ID)
	}

	assert.Equal(t, expiredFileIDs, gcFileIDs)

}
func setupTestDatabase() (*gorm.DB, error) {
	cxn := ":memory:?cache=shared"
	return gorm.Open(sqlite.Open(cxn))
}
