package repo

import (
	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"gorm.io/gorm"
	"time"
	"timebox/model"
)

type FilesRepository struct {
	DB *gorm.DB
}

func Migrate(db *gorm.DB) {
	err := db.AutoMigrate(&model.File{})
	if err != nil {
		log.Fatal("Failed to migrate")
	}
}

func (r *FilesRepository) NewFile(file *model.File) error {
	return r.DB.Create(file).Error
}

func (r *FilesRepository) GetFile(fileID *uuid.UUID) (*model.File, error) {
	var file model.File
	err := r.DB.First(&file, "id = ?", fileID).Error
	if err != nil {
		return nil, err
	}
	return &file, nil
}

func (r *FilesRepository) GetFilesByAlias(alias string) (*[]model.File, error) {
	files := make([]model.File, 10)
	err := r.DB.Find(&files, "alias = ?", alias).Error
	if err != nil {
		return nil, err
	}
	return &files, nil
}

func (r *FilesRepository) IncrDownloadCount(fileID uuid.UUID) {
	var file model.File
	err := r.DB.First(&file, "id = ?", &fileID).Error
	if err != nil {
		return
	}
	err = r.DB.Model(&file).Update("Downloads", file.Downloads+1).Error
	if err != nil {
		log.Error(err)
	}
	if file.Downloads >= file.MaxDownloads || file.ExpiresAt.Before(time.Now()) {
		err := r.DB.Delete(&file).Error
		if err != nil {
			log.Error(err)
		}
	}
}

func (r *FilesRepository) SoftDeleteFile(fileID uuid.UUID) {
	var file model.File
	err := r.DB.First(&file, "id = ?", &fileID).Error
	if err != nil {
		return
	}
	if file.Downloads >= file.MaxDownloads || file.ExpiresAt.Before(time.Now()) {
		err := r.DB.Delete(&file).Error
		if err != nil {
			log.Error(err)
		}
		log.Warnf("File Deleted %v", file.Path)
	}
}

func (r *FilesRepository) BulkPermDelete(files *[]model.File) error {
	return r.DB.Unscoped().Delete(files).Error
}

func (r *FilesRepository) GC() []model.File {
	files := make([]model.File, 10)
	r.DB.Unscoped().Model(&model.File{}).Find(&files, "deleted_at IS NOT NULL OR expires_at < ? OR downloads >= max_downloads", time.Now())
	return files
}
