package routes

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"mime/multipart"
	"net/http"
	"strconv"
	"strings"
	"time"
	"timebox/model"
	"timebox/repo"
	"timebox/storage"
)

type UploadForm struct {
	TTL          string                `form:"ttl" binding:"required"`
	MaxDownloads string                `form:"max_downloads" binding:"required"`
	Alias        string                `form:"alias"`
	File         *multipart.FileHeader `form:"file" binding:"required"`
}

func handleUpload(c *gin.Context) {
	storageService, ok := c.Value(MiddlewareStorageKey).(*storage.Storage)
	if !ok {
		c.JSON(http.StatusInternalServerError, NewApiResponseError(ServerError, &ServerFailure{}))
		return
	}

	filesRepository, ok := c.Value(MiddlewareFileManagerRepoKey).(*repo.FilesRepository)
	if !ok {
		c.JSON(http.StatusInternalServerError, NewApiResponseError(ServerError, &ServerFailure{}))
		return
	}
	var form UploadForm
	// in this case proper binding will be automatically selected
	if err := c.ShouldBind(&form); err != nil {
		c.JSON(http.StatusBadRequest, NewApiResponseError(MissingFormData, errors.New("required fields missing in form")))
		return
	}
	contextType := form.File.Header.Get("Content-Type")
	ttl, err := time.ParseDuration(form.TTL)
	if err != nil {
		c.JSON(http.StatusBadRequest, NewApiResponseError(ParseError, err))
		return
	}
	maxDownloads, err := strconv.Atoi(form.MaxDownloads)
	if err != nil {
		c.JSON(http.StatusBadRequest, NewApiResponseError(ParseError, err))
		return
	}
	var alias *string
	aliasParam := strings.TrimSpace(form.Alias)
	if len(aliasParam) > 0 {
		alias = &aliasParam
	}

	src, err := form.File.Open()

	if err != nil {
		c.JSON(http.StatusBadRequest, NewApiResponseError(FileError, err))
		return
	}
	defer func() {
		err := src.Close()
		if err != nil {
			log.Tracef("Failed to close multipart file %s", err)
		}
	}()

	fileID, err := uuid.NewUUID()
	if err != nil {
		c.JSON(http.StatusInternalServerError, NewApiResponseError(ServerError, err))
		return
	}
	fileKey := fmt.Sprintf("files/%s", fileID)
	expiryDate := time.Now().Add(ttl)
	metadata, err := storageService.Put(fileKey, src, expiryDate)
	if err != nil {
		c.JSON(http.StatusInternalServerError, NewApiResponseError(ServerError, err))
		return
	}

	log.Tracef("File Uploaded Succesfully %s", fileKey)
	uploadedFile := model.File{
		ID:           fileID,
		Path:         fileKey,
		Alias:        alias,
		MaxDownloads: maxDownloads,
		ExpiresAt:    metadata.Created.Add(ttl),
		CreatedAt:    metadata.Created,
		Size:         metadata.ContentLength,
		ContentType:  contextType,
	}
	err = filesRepository.NewFile(&uploadedFile)
	if err != nil {
		c.JSON(http.StatusInternalServerError, NewApiResponseError(ServerError, &ServerFailure{}))
		return
	}
	log.Tracef("File Saved to Database Succesfully %s", fileKey)

	c.JSON(http.StatusOK, ApiResponse{
		Data:  &uploadedFile,
		Error: nil,
	})
}
