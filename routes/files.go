package routes

import (
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"net/http"
	"strings"
	"time"
	"timebox/model"
	"timebox/repo"
	"timebox/storage"
)

const (
	ParamFileID = "fileID"
	ParamAlias  = "alias"
)

func handleDownloadFile(c *gin.Context) {
	fileIDParam := c.Param(ParamFileID)
	fileID, err := uuid.Parse(fileIDParam)
	if err != nil {
		c.Data(http.StatusBadRequest, "", []byte{})
		return
	}
	storageService, ok := c.Value(MiddlewareStorageKey).(*storage.Storage)
	if !ok {
		c.Data(http.StatusNotFound, "", []byte{})
		return
	}

	filesRepository, ok := c.Value(MiddlewareFileManagerRepoKey).(*repo.FilesRepository)
	if !ok {
		c.JSON(http.StatusInternalServerError, NewApiResponseError(ServerError, &ServerFailure{}))
		return
	}

	file, err := filesRepository.GetFile(&fileID)
	if err != nil || file == nil {
		c.Data(http.StatusNotFound, "", []byte{})
		return
	}
	if file.ExpiresAt.Before(time.Now()) {
		defer filesRepository.SoftDeleteFile(fileID)
		c.Data(http.StatusNotFound, "", []byte{})
		return
	}
	download, err := storageService.Get(c, file.Path)
	if err != nil {
		c.Data(http.StatusNotFound, "", []byte{})
		return
	}

	c.DataFromReader(http.StatusOK, file.Size, file.ContentType, download, map[string]string{})

	defer func() {
		filesRepository.IncrDownloadCount(fileID)
		err := download.Close()
		if err != nil {
			log.Warn(err)
		}
	}()
}
func handleListFiles(c *gin.Context) {
	aliasParam := strings.TrimSpace(c.Query(ParamAlias))
	fileIDParam := strings.TrimSpace(c.Query(ParamFileID))

	filesRepository, ok := c.Value(MiddlewareFileManagerRepoKey).(*repo.FilesRepository)
	if !ok {
		c.JSON(http.StatusInternalServerError, NewApiResponseError(ServerError, &ServerFailure{}))
		return
	}

	if len(fileIDParam) > 0 {
		fileID, err := uuid.Parse(fileIDParam)
		if err != nil {
			c.JSON(http.StatusBadRequest, ApiResponse{
				Data:  &[]model.File{},
				Error: nil,
			})
			return
		}
		file, err := filesRepository.GetFile(&fileID)
		if err != nil || file == nil {
			c.Data(http.StatusNotFound, "", []byte{})
			return
		}
		c.JSON(http.StatusOK, ApiResponse{
			Data:  &[]model.File{*file},
			Error: nil,
		})
		return
	}

	if len(aliasParam) > 0 {
		files, err := filesRepository.GetFilesByAlias(aliasParam)
		if err != nil {
			c.JSON(http.StatusOK, ApiResponse{
				Data:  &[]model.File{},
				Error: nil,
			})
			return
		}
		c.JSON(http.StatusOK, ApiResponse{
			Data:  files,
			Error: nil,
		})
		return
	}

	c.JSON(http.StatusBadRequest, NewApiResponseError(MissingFormData, errors.New("Query params [alias or fileID] missing ")))
}
