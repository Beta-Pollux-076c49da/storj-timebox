package routes

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"timebox/repo"
	"timebox/storage"
)

const (
	MiddlewareStorageKey         = "storage"
	MiddlewareFileManagerRepoKey = "fileRepo"
)

func RegisterRoutes(r *gin.Engine, s *storage.Storage, db *gorm.DB) {

	v1 := r.Group("/v1")

	{
		v1.Use(StorageMiddleWare(s))
		v1.Use(FilesRepositoryMiddleWare(db))
		v1.POST("/upload", handleUpload)
		v1.GET("/files/:fileID", handleDownloadFile)
		v1.GET("/files", handleListFiles)
	}
}

func StorageMiddleWare(s *storage.Storage) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set(MiddlewareStorageKey, s)
		c.Next()
	}
}

func FilesRepositoryMiddleWare(db *gorm.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		fm := repo.FilesRepository{
			DB: db.WithContext(c),
		}
		c.Set(MiddlewareFileManagerRepoKey, &fm)
		c.Next()
	}
}

type ApiErrorCode int8

const (
	MissingFormData ApiErrorCode = iota + 1
	FileError
	ParseError
	ServerError
)

type ServerFailure struct {
}

func (s *ServerFailure) Error() string {
	return "Server error"
}

type ApiError struct {
	Message string       `json:"message"`
	Code    ApiErrorCode `json:"code"`
}

func NewApiResponseError(code ApiErrorCode, err error) ApiResponse {
	apiError := ApiError{
		Code:    code,
		Message: err.Error(),
	}

	return ApiResponse{
		Data:  nil,
		Error: &apiError,
	}
}

type ApiResponse struct {
	Data  interface{} `json:"data"`
	Error *ApiError   `json:"error"`
}
