package config

import (
	log "github.com/sirupsen/logrus"
	"os"
)

/*
ACCESS_GRANT
BUCKET_NAME
GC_CRON
DATABASE
DATABASE_URL
*/
type Environment struct {
	AccessGrant string
	BucketName  string
	GC          string
	Database    DatabaseOption
	DatabaseUri string
	ServerHost  string
	ServerPort  string
	GinMode     string
}
type DatabaseOption int

const (
	SQLITE DatabaseOption = iota + 1
	POSTGRES
	INVALID
)

func ParseDatabaseOption(s string) DatabaseOption {
	if s == "postgres" {
		return POSTGRES
	} else if s == "sqlite" {
		return SQLITE
	}
	return INVALID
}

func GetEnvOrDefault(key string, defaultValue string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return defaultValue
	}
	return value
}

var Env *Environment

func EnsureEnv() {
	accessGrant := os.Getenv("ACCESS_GRANT")
	bucketName := os.Getenv("BUCKET_NAME")
	cron := os.Getenv("GC_CRON")
	database := ParseDatabaseOption(os.Getenv("DATABASE"))
	databaseUri := os.Getenv("DATABASE_URI")
	host := GetEnvOrDefault("HOST", "localhost")
	port := GetEnvOrDefault("PORT", "8080")
	mode := GetEnvOrDefault("GIN_MODE", "debug")

	if len(accessGrant) == 0 {
		log.Fatal("ACCESS_GRANT not found in your current environment")
	}
	if database == INVALID {
		log.Fatal("DATABASE option invalid, possible choices [sqlite,postgres]")
	}
	if len(databaseUri) == 0 {
		log.Fatal("DATABASE_URI not found in your current environment")
	}

	if len(bucketName) == 0 {
		bucketName := "timebox"
		log.Warnf("BUCKET_NAME name not found, Setting default %s", bucketName)
	}
	if len(cron) == 0 {
		cron := "@every 30m"
		log.Warnf("GC_CRON name not found, Setting default %s", cron)
	}

	Env = &Environment{
		AccessGrant: accessGrant,
		BucketName:  bucketName,
		GC:          cron,
		Database:    database,
		DatabaseUri: databaseUri,
		ServerHost:  host,
		ServerPort:  port,
		GinMode:     mode,
	}
}
