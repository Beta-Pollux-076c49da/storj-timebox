# Storj Timebox

## Build and Run
To run Timebox server you need to set these environmental variables on your server environment or pass an env file to the program 

```dotenv
ACCESS_GRANT = "STORJ_ACCESS_GRANT" #required
BUCKET_NAME = "STORJ_BUCKET_NAME" #optional default(timebox)
GC_CRON = "CRON_JOB_SCHEDULE_FOR_GC"  #optional default(@every 10s) refer to https://pkg.go.dev/github.com/robfig/cron
DATABASE = "DATABASE_OPTION" #required possible value [sqlite,postgres] 
DATABASE_URI = "DATABASE_CONNECTION_STRING_OR_PATH" #required
HOST = "localhost" #optional default(localhost)
PORT = "8080" #optional default(localhost) 
GIN_MODE="debug" #optional
```
### Install Go
[Go Lang Download](https://golang.org/dl/)
### Clone
```shell
git clone https://gitlab.com/Beta-Pollux-076c49da/storj-timebox.git timebox
```
```shell
cd timebox
```
### Build
```shell
go build -o timebox
```
### Start Timebox Service
```shell
./timebox --env=OPTIONAL_ENV_FILE
```

### Running the demo
```shell
./timebox --env=demo.env
```
### Demo.evn
```dotenv
ACCESS_GRANT = "18yMsZpg6ZQdzDXWGjzK4VNF4M9CPJABpE18EwPtuq3eeu1n61sjm4RQYYAU5zqrBn453niDD4HLpCoRABMrEVdm1AvjjpRi7rjfE8eHfAN8YCx8zzJpTgZ9h4XRx7KBEM1RSvDNsFpiVA23kW9XU7sNbQ4GFZiZNShASFmGmS6CLPQbEDwf4UUN9Y2LGULoqS1iiHoyS9ZFeAztwaP2RkrYnK9dNbL6WwPTPrCZP4UXCyhWpiTxavFzfp4UgmCNGpKDQvsuu4C"
BUCKET_NAME = "anonymous"
GC_CRON = "@every 10s"
DATABASE = "sqlite"
DATABASE_URI = "timebox.db"
HOST = "localhost"
PORT = "8080"
```

## Running in Production
### Example Env
```dotenv
ACCESS_GRANT = "18yMsZpg6ZQdzDXWGjzK4VNF4M9CPJABpE18EwPtuq3eeu1n61sjm4RQYYAU5zqrBn453niDD4HLpCoRABMrEVdm1AvjjpRi7rjfE8eHfAN8YCx8zzJpTgZ9h4XRx7KBEM1RSvDNsFpiVA23kW9XU7sNbQ4GFZiZNShASFmGmS6CLPQbEDwf4UUN9Y2LGULoqS1iiHoyS9ZFeAztwaP2RkrYnK9dNbL6WwPTPrCZP4UXCyhWpiTxavFzfp4UgmCNGpKDQvsuu4C"
BUCKET_NAME = "anonymous"
GC_CRON = "@every 10s"
DATABASE = "postgres"
DATABASE_URI = "host=localhost user=postgres password=postgres dbname=timebox port=5432 sslmode=disable"
HOST = "0.0.0.0"
PORT = "80"
GIN_MODE="release"
```