package model

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
	"time"
)

type File struct {
	ID           uuid.UUID       `gorm:"primaryKey" json:"id"`
	Path         string          `gorm:"index" json:"path"`
	Alias        *string         `gorm:"index" json:"alias"`
	MaxDownloads int             `json:"max_downloads"`
	Downloads    int             `json:"downloads"`
	Size         int64           `json:"size"`
	ContentType  string          `json:"content_type"`
	ExpiresAt    time.Time       `gorm:"index" json:"expires_at"`
	CreatedAt    time.Time       `json:"created_at"`
	DeletedAt    *gorm.DeletedAt `json:"deleted_at"`
}
