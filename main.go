package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"github.com/robfig/cron"
	log "github.com/sirupsen/logrus"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"timebox/config"
	"timebox/repo"
	"timebox/routes"
	"timebox/storage"
)

func main() {
	envFile := flag.String("env", ".env", "Specifies env file")
	flag.Parse()
	err := godotenv.Load(*envFile)
	if err != nil {
		log.Warnf(fmt.Sprintf("Error loading .env file %s", err))
	}
	config.EnsureEnv()

	s, err := storage.RegisterStorage(context.Background())
	if err != nil {
		log.Fatal(fmt.Sprintf("Error registering storj dependency %s", err))
	}
	db, err := OpenDatabase()
	repo.Migrate(db)
	if err != nil {
		panic("failed to connect database")
	}

	gin.SetMode(config.Env.GinMode)
	r := gin.Default()
	routes.RegisterRoutes(r, s, db)

	c := cron.New()
	err = c.AddFunc(config.Env.GC, func() {
		RunGarbageCollection(db, s)
	})
	if err != nil {
		log.Fatal(err)
	}
	c.Start()
	defer c.Stop()

	err = r.Run(fmt.Sprintf("%s:%s", config.Env.ServerHost, config.Env.ServerPort))
	log.Fatal(err)
}

func OpenDatabase() (*gorm.DB, error) {
	switch config.Env.Database {
	case config.SQLITE:
		return gorm.Open(sqlite.Open(config.Env.DatabaseUri))
	case config.POSTGRES:
		return gorm.Open(postgres.Open(config.Env.DatabaseUri))
	}
	return nil, errors.New("invalid database option")
}

func RunGarbageCollection(db *gorm.DB, sm *storage.Storage) {
	fm := repo.FilesRepository{DB: db}
	garbage := fm.GC()
	if len(garbage) <= 0 {
		return
	}
	for _, file := range garbage {
		err := sm.Delete(context.Background(), file.Path)
		if err != nil {
			log.Warnf("Failed to delete file %v from storage", file.ID)
			break
		}
	}

	err := fm.BulkPermDelete(&garbage)
	if err != nil {
		log.Error("Failed to delete garbage from database")
		return
	}

	log.Infof("GC Finished removed %v items", len(garbage))

}
