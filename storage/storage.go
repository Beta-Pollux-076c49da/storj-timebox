package storage

import (
	"context"
	"io"
	"mime/multipart"
	"storj.io/uplink"
	"time"
	"timebox/config"
)

type Storage struct {
	project *uplink.Project
	bucket  *uplink.Bucket
}

func RegisterStorage(c context.Context) (*Storage, error) {
	accessGrant := config.Env.AccessGrant
	bucketName := config.Env.BucketName

	access, err := uplink.ParseAccess(accessGrant)
	if err != nil {
		return nil, err
	}

	project, err := uplink.OpenProject(c, access)
	if err != nil {
		return nil, err
	}
	bucket, err := project.EnsureBucket(c, bucketName)

	if err != nil {
		return nil, err
	}

	storage := Storage{
		project: project,
		bucket:  bucket,
	}
	return &storage, nil
}

func (s *Storage) Put(key string, file multipart.File, ttl time.Time) (*uplink.SystemMetadata, error) {
	options := uplink.UploadOptions{
		Expires: ttl.UTC(),
	}
	upload, err := s.project.UploadObject(context.Background(), s.bucket.Name, key, &options)
	_, err = io.Copy(upload, file)
	if err != nil {
		return nil, err
	}

	err = upload.Commit()
	if err != nil {
		return nil, err
	}
	return &upload.Info().System, nil
}

func (s *Storage) Get(c context.Context, key string) (*uplink.Download, error) {
	return s.project.DownloadObject(c, s.bucket.Name, key, nil)
}

func (s *Storage) Delete(c context.Context, key string) error {
	_, err := s.project.DeleteObject(c, s.bucket.Name, key)
	if err != nil {
		return err
	}
	return nil
}
